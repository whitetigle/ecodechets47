module SecondLife.View

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Browser

type GarbageCollector = Yellow | Specialized | Green | Glass

type Garbage = {
  what:   string
  where:  GarbageCollector
  about : string
  hint :  string option
}

let metalList = [
  { 
    what= "Aérosols"
    where=Yellow
    about= "Les aérosols en métal de produits ménagers ou d'hygiène se recyclent tous et se trient bien vidés, avec leur couvercle/capuchon. Exemples : Aérosols dépoussiérant, désodorisant, mousse à raser, déodorant, laque. ATTENTION ! Sauf les aérosols de produits dangereux qui sont des déchets chimiques des ménages et sont donc à déposer en déchetterie."
    hint=Some "Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal."
  }
  {
    what= "Vaisselle en métal"
    where=Specialized
    about= "Les plats, plateaux, casseroles, poêles, pots, gourdes, fourchettes, couteaux et cuillères en métal sont à déposer en déchetterie."
    hint=None
  }
  {
    what= "Pots en métal"
    where=Yellow
    about= "Les pots de crème et de baume à lèvres en métal se trient avec leur couvercle."
    hint=None
  }
  {
    what="Tubes souples en métal"
    where=Yellow
    about="Les tubes en métal de dentifrice, de sauce tomate et même de cirage ou autres produits d'entretiens se recyclent tous et se trient bien vidés sans leur bouchon."
    hint=Some"Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal"
  }
  {
    what= "Cubitainers et bidons en métal"
    where=Yellow
    about= "Les cubitainers et bidons en métal se recyclent et peuvent être triés bien vidés, avec leur bouchon. Exemple : Les cubitainers cylindriques en métal de bière."
    hint= Some "Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal."
  }
  {
    what= "Boîtes en métal"
    where=Yellow
    about= "Toutes les boîtes de conserve (Boîtes de maïs, d'haricots, de sardines, de thon, de pâté,…) et les autres boîtes en métal (Boîtes de cirage,…) se recyclent et se trient avec leur couvercle mais bien vidées, inutile de rincer."
    hint=Some "Avec 9 boîtes de conserve recyclées, on fabrique 1 boule de pétanque… Et avec 7849 boîtes de conserves recyclées, on fabrique un chariot de supermarché."
  }
  {
    what= "Couvercles en métal"
    where=Yellow
    about= "Tous les couvercles en métal se recyclent mais se trient séparément du contenant lorsque celui-ci est en verre. Exemples : Les couvercles en métal de pot de cornichons, de pots de confiture,…"
    hint= Some "Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal."
  }
  {
    what=  "Barquettes en métal"
    where=Yellow
    about= "Les barquettes en métal de plats cuisinés et les plats en métal se recyclent et se trient bien vidés, inutile de laver. Exemple : Les barquettes en métal de gratin."
    hint=None
  }
  {
    what= "Bouteilles en métal"
    where=Yellow
    about= "Toutes les bouteilles en métal se recyclent et se trient quel que soit leur format, bien vidées et avec leur bouchon. Exemples : Les bouteilles en métal de sirop ou de grenadine…"
    hint=Some "Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal."
  }
  {
    what = "Bouchons et capsules en métal"
    where = Yellow
    about=  "Les bouchons et les capsules en métal de bouteilles se recyclent tous. Souvent associés à des bouteilles en verre, ils se trient séparément. Exemples : Les bouchons en métal de bouteille d'eau gazeuse en verre ou capsule en métal de bouteille de bière en verre."
    hint = Some "Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal."  
  }
]

let paperList = 
  [
    {
      what =  "Feuilles, lettres et enveloppes"
      where = Yellow
      about =  "Tous les papiers se recyclent quelle que soit leur taille. Exemples : Feuilles et papiers, lettres et courriers (laissez les agrafes s'il y en a, ce n'est pas gênant pour le processus de tri) et les enveloppes même celles avec une fenêtre en plastique."
      hint = None
    }
    {
      what =  "Cartes, tickets et étiquettes"
      where = Yellow
      about =  "Toutes les papiers cartonnés se recyclent. Exemples : Les cartes postales, les billets d'avion, de train, les tickets de metro, les jeux à gratter, les cartes à jouer, les étiquettes d'achat de vêtement ou d'emballage non adhésives…"
      hint = None
    }
    {
      what =  "Livres, manuels, notices"
      where = Yellow
      about =  "Tous les livres en trop mauvais état pour être partagés, même avec une couverture rigide se recyclent."
      hint = None
    }
    {
      what =  "Journaux, magazines et prospectus"
      where = Yellow 
      about =  "Tous les papiers, journaux, magazines et prospectus se recyclent et se trient sans leur film plastique."
      hint = None
    }
    {
      what =  "Affiches et posters"
      where = Yellow 
      about =  "Les affiches, posters et publicités papier quelle que soit leur taille se recyclent."
      hint = None
    }
    {
      what =  "Papiers kraft"
      where = Yellow
      about =  "Les papiers et enveloppes en kraft se recyclent."
      hint = None
    }
    {
      what =  "Papiers calque"
      where = Green
      about =  "Les papiers calque et papiers semi transparent sont à jeter aux ordures ménagères."
      hint = None
    }
    {
      what =  "Feuilles d'aluminium"
      where = Yellow
      about =  "Les feuilles d'aluminium qui enveloppent les produits alimentaires se recyclent et se trient même souillés. Exemples : Les feuilles d'aluminium des portions de fromage Kiri ou La vache qui rit… Mais aussi toutes les feuilles d'aluminium que vous achetez en rouleau pour envelopper vos aliments."
      hint= Some "Grâce au recyclage de l'aluminium, on économise 95% de la matière première et 95% de l'énergie nécessaire à la production du métal."
    }
    {
      what =  "Papiers photo"
      where = Green
      about =  "Les papiers et tirages photos sont à jeter aux ordures ménagères car ils n'ont de papier que le nom."
      hint = None
    }
    {
      what =  "Papiers cuisson"
      where = Green
      about =  "Les papiers sufulrisés et les feuilles de cuisson sont à jeter aux ordures ménagères."
      hint = None
    }
    {
      what =  "Papiers peint"
      where = Green
      about =  "Le papier peint, le papier mural et décoratif sont à jeter aux ordures ménagères."
      hint = None 
    }
    {
      what =  "Papiers bulles"
      where = Green
      about =  "Les papiers bulles utilisés dans les colis que vous recevez pour protéger vos objets ou lors de déménagement. En grande quantité, vous pouvez les déposer en déchetterie"
      hint = None
    }
  ]

let cartonList = 
  [
    {
      what =  "Cubitainers et bidons en carton"
      where = Yellow
      about =  "Les cubitainers et bidons en carton se recyclent et peuvent être triés bien vidés, avec leur bouchon. Attention, le pochon en plastique à l'intérieur est à retirer et à jeter aux ordures ménagères. Exemples : Les cubitainers rectangulaires en carton de vins et rosés."
      hint = None
    }
    {
      what =  "Barquettes en carton"
      where = Yellow
      about =  "Les barquettes en carton se recyclent et se trient bien vidées, même souillées de graisses ou de jus. Exemples : Barquettes en carton de frites, de fruits frais,…"
      hint = None
    }
    {
      what =  "Boîtes en carton"
      where = Yellow
      about =  "Toutes les boîtes en carton de produits alimentaires, de cosmétiques, de jouets et jeux, ou de produits ménagers se recyclent et sont à trier bien vidées, même salies. Vous pouvez les trier avec leur couvercle et laisser les films ou fenêtres en plastique qui y sont intégrés parfois comme sur les boîtes de mouchoirs jetables ou sur les boîtes d'emballage de poupées Barbie® par exemple. Autres exemples : Les boîtes en carton de pizzas souillées mais bien vidées, les boîtes de gâteaux, de céréales, de spaghettis, de dentifrice, de parfum, de lessive en poudre, de pastilles pour lave-vaisselle, de chaussures,..."
      hint = Some "Avec 4 boîtes en carton de céréales recyclées, on fabrique 1 boîte à chaussures."
    }
    {
      what =  "Grands cartons"
      where = Specialized
      about =  "Les cartons de déménagement, de produits High Tech (Téléviseur, ordinateur,…) ou électroménagers (Lave-linge, lave-vaisselle, four,...) sont à déposer en déchetterie : ils se recyclent bien mais sont trop encombrants pour être collectés avec vos emballages. Renseignez-vous auprès de votre collectivité pour connaître les horaires d'ouverture."
      hint = None
    }
    {
      what =  "Cartonnettes et étuis"
      where = Yellow
      about =  "Toutes les cartonnettes d'emballage et autres petits étuis en cartons se recyclent et se trient même souillés. Exemple : Cartonnette des packs de yaourts, étuis en carton de tranches de magret de canard fumé, cartonnette de maintien des gobelets de fast food,…"
      hint = None
    }
    {
      what =  "Rouleaux en carton"
      where = Yellow
      about =  "Les rouleaux en carton de papier cuisson, de papier aluminium, de papier essuie-tout et de papier toilette se recyclent"
      hint = None
    }
    {
      what = "Briques alimentaires"
      where = Yellow
      about =  "Toutes les Briques alimentaires en carton, bien vidées, inutile de rincer et avec leur bouchon en plastique s'il y en a un. Exemples : Les briques de lait, de soupe, de béchamel, de jus de fruits.."
      hint = None 
    }
    {
      what = "Berlingots en carton"
      where = Yellow 
      about =  "Les berlingots en carton se recyclent et se trient bien vidés, inutile de rincer. Exemples : Les berlingots en carton de de lait concentré sucré, de yaourt à boire ou de smoothie"
      hint = None
    }
    {
      what =  "Pots en papier/carton"
      where = Yellow
      about =  "Les petits pots de glace,... en papier carton se recyclent et se trient souillés mais bien vidés (inutile de rincer)."
      hint = None
    }
  ]

let plasticList = 
  [
    {
      what = "Cubitainers et bidons en plastique"
      where = Yellow
      about =  "Les cubitainers et bidons en plastique se recyclent et peuvent être triés bien vidés, avec leur bouchon. Exemples : Les cubitainers en plastique de vins, les bidons en plastique de nettoyant ou de vinaigre ménager,… ATTENTION ! Sauf les bidons de produits dangereux."
      hint = None
    }
    {
      what = "Boîtes en plastique"
      where = Green
      about = "Boîtes en plastique de produits alimentaires, ménagers ou de cosmétiques. Exemples : Boîtes d'œufs en plastique, boîtes de Bretzel, de cotons-tiges, de beurre, de salades composées ou de bâtons de surimi en plastique…"
      hint = None
    }
    {
      what = "Boîtes en plastique (Contenu sec)"
      where = Yellow
      about = "Certaines boîtes en plastique ayant contenu des produits secs se recyclent et vous pouvez les trier avec leur couvercle. Exemples : Les boîtes en plastique de chocolat en poudre Nesquik® et Poulain®, la 'box' de Belin® (Assortiment de crackers) ou encore la boîte ronde de bonbons Haribo®."
      hint = None
    }
    {
      what = "Films en plastique"
      where = Green
      about = "Les films en plastique entourant les packs et les lots. Exemples : Les films en plastique de packs d'eau, de lait, de cannettes de sodas, de berlingots ménagers ou d'hygiène."
      hint = None
    }
    {
      what = "Jouets en plastique"
      where = Green
      about = "S'ils sont en bon état, faites don de vos jouets en plastique (hochets, poupées,…) auprès d'associations. Certaines déchetteries les acceptent (renseignez-vous auprès de votre collectivité). Sinon, jetez-les aux ordures ménagères, ils ne se recyclent pas."
      hint = None
    }
    {
      what = "Poches et sachets en plastique"
      where = Green
      about = "Toutes les poches et sachets en plastique (transparent ou opaque) d'emballage. Exemples : Sachet de paquet de chips, sachet de paquet de céréales, sachet de pâte feuilletée, sachets enveloppant les magazines, prospectus et journaux…"
      hint = None
    }
    {
      what = "Poches souples en plastique"
      where = Green
      about = "Les poches souples en plastique avec leur bouchon : Exemples : Les poches de compote à boire Pom'Potes® ou de sucre en poudre Béghin Say®."
      hint = None
    }
    {
      what = "Sacs en plastique"
      where = Green
      about = "Les sacs en plastique opaques ou transparents pour transporter vos courses, vos fruits et légumes,…"
      hint = None
    }
    {
      what = "Pots en plastique"
      where = Green
      about = "Exemples : Les pots en plastique de yaourt, de crème fraiche, de fromage blanc mais aussi les pots en plastique de crèmes cosmétiques"
      hint = None
    }
    { 
      what = "Ruban plastique ou tissu"
      where = Green
      about = "Les rubans cadeau et fermeture de boîtes en plastique en tissu sont à jeter aux ordures ménagères."
      hint = None
    }
    {
      what = "Sacs en plastique"
      where = Green
      about = "Les sacs en plastique opaques ou transparents pour transporter vos courses, vos fruits et légumes,…"
      hint = None
    }
    {
      what = "Tubes souples en plastique"
      where = Green
      about = "Les tubes souples en plastique de produits alimentaires, cosmétiques ou ménagers. Exemples : Tubes en plastique de lait concentré, de mayonnaise, de dentifrice, de crème hydratante, de protection solaire, de lessive liquide,…"
      hint = None
    }
    {
      what = "Barquettes en plastique"
      where = Green
      about = "Exemples : Les barquettes en plastique de biscuits, de gateaux, de plats cuisinés, de plats surgelés, de produits traiteur (traiteur asiatique,...), de charcuterie (jambon, lardons,...), de steak haché"
      hint = None
    }
  ]

let glassList = 
  [
    {
      what = "Vaisselle"
      where = Specialized
      about = "Que ce soit en verre, verre Pyrex®, porcelaine, vitrocéramique : Les assiettes, verres de table, coupes, flûtes à champagne, vases et plats ne se recyclent pas, ils sont à déposer en déchetterie. Si vous avez cassé un verre ou un plat, évitez de le mettre dans les ordures ménagères, cela peut être dangereux pour les agents de collecte (éboueurs) car ils peuvent se couper en manipulant les sacs."
      hint = None
    }
    {
      what = "Bouteilles en verre"
      where = Glass
      about = "Toutes les bouteilles en verre se recyclent et se trient quel que soit leur format, bien vidées mais sans leur bouchon en métal, liège ou plastique qui se trient séparément (Vous pouvez cependant laisser le collier du bouchon sur la bouteille...). Exemples : Bouteilles en verre de jus de fruits, de soda, de vin, de cidre, de bière, d'huile, de vinaigre…"
      hint = Some "Avec une bouteille en verre, on fabrique une autre bouteille en verre et ainsi de suite car le verre se recycle à l'infini…"
    }
    {
      what = "Pots et bocaux en verre"
      where = Glass
      about = "Tous les pots et bocaux en verre se recyclent et se trient bien vidés sans leur couvercle. Vous pouvez laisser l'étiquette. Exemples : Les pots et bocaux en verre de confiture, de pâte à tartiner, de cornichons (sans la plateforme en plastique), de mayonnaise, de moutarde, de sauce tomate..."
      hint = Some "Le verre se recycle à 100% et à l'infini ! Un pot en verre recyclé produira un nouveau pot en verre"
    }
    {
      what = "Flacons billes en verre"
      where = Glass
      about = "Les flacons de déodorants en verre avec bille d'application se recyclent et se trient avec leur bouchon"
      hint = None
    }
    {
      what = "Flacons en verre"
      where = Glass 
      about = "Les flacons ou flacons-spray en verre de parfum, d'eau de cologne ou de fond de teint se recyclent et se trient avec leur bouchon."
      hint = None
    }
    {
      what = "Verre brisé"
      where = Specialized
      about = "Le verre brisé, les miroirs et autres glaces réfléchissantes, carreaux, vitres, rétroviseurs, fenêtres cassées ne se recyclent pas, ils sont à déposer en déchetterie. Si vous avez cassé un verre, un plat ou un vase, évitez de le mettre dans les ordures ménagères, cela peut être dangereux pour les agents de collecte (éboueurs) car ils peuvent se couper en manipulant les sacs."
      hint = None    
    }
  ]

type Topic = {
  title : string
  icon : string
  list : Garbage list
}

let topics = [
  {
    title = "Le verre"
    icon = "glass"
    list = glassList
  }
  {
    title = "Le carton"
    icon = "map-o"
    list = cartonList
  }
  {
    title = "Le plastique"
    icon = "cubes"
    list = plasticList
  }
  {
    title = "Le papier"
    icon = "book"
    list = paperList
  }
  {
    title = "Le métal"
    icon = "cogs"
    list = metalList
  }
]

let mkCard icon info = 
  let mkHint (hint:string option) =
    if hint.IsNone then span[][]
    else
      div [ ClassName "card-action amber lighten-2" ]
        [ 
          h5 [ ClassName "black-text kanit"] [str "Le saviez-vous?"]
          p [  ClassName "dark-green-text darken-5-text kanit"] [str hint.Value ]
        ]

  let mkRecycle where = 
    let icon, text = 
      match where with 
      | Green -> ("exclamation-triangle","À jeter!")
      | Specialized -> ("recycle","Déposer dans une déchèterie")
      | Glass ->  ("trash","Déposer dans un point de collecte")
      | Yellow ->  ("trash-o","Déposer dans un point de collecte ou bac au couvercle jaune")

    h5 
      [ ClassName "light-green kanit" ]
      [
        span
          [ ClassName (sprintf "white-text fa fa-%s" icon) ]
          []
        span 
          [ ClassName "white-text darken-4-text"]
          [str (sprintf " %s" text ) ]
      ]

  div 
    [ ClassName "row" ]
    [
      div 
        [ ClassName "col s12" ]
        [
          div 
            [ ClassName "card light-green lighten-2 hoverable" ]
            [ 
              div [ClassName  "card-content black-text" ]
                [
                  span [ClassName "card-title" ] 
                    [ 
                      span
                        [ ClassName (sprintf "fa fa-%s" icon) ]
                        []
                      str (sprintf " %s" info.what )
                    ]
                  p [ ClassName ""] [ mkRecycle info.where ]
                  p [] [ str info.about ]
                ]
              mkHint info.hint
            ]
        ]
    ]

let getContents clist icon = 
  div 
    [ ]
    (clist 
      |> Seq.map (mkCard icon)
      |> Seq.toList)

let mkTtile t icon count= 
  [
    i
      [ ClassName (sprintf "fa fa-%s" icon) ]
      []
    span
      [ ClassName "new badge";  unbox("data-badge-caption","articles") ]
      [ str (sprintf "%i" count)]
    h4 
      []
      [str t]      
  ]

let root =
  let initAccordion element = 
    let nav = Global.jQuery ".collapsible"
    nav ? collapsible () |> ignore  

  let mkLi inf = 
    let count = inf.list |> Seq.length
    li 
      []
      [ 
        div [ ClassName "collapsible-header kanit"] 
          (mkTtile inf.title inf.icon count)
        div [ ClassName "collapsible-body"] 
          [ 
            span [] [(getContents inf.list inf.icon)]
          ]
      ]


  div
    [ ClassName "row center"]
    [
      h4 
        [ ClassName "red-text text-lighten-2"]
        [ str "EcoDecheTs47: Valorisation des déchêts"]
      p 
        [ ClassName "kanit"]
        [str "Découvrez où recycler vos déchêts et ce qu'ils deviennent une fois recyclés !"]
      ul
        [ ClassName "collapsible left-align"; 
          unbox("data-collapsible","accordion");
          Ref initAccordion 
        ]
        (topics |> Seq.map mkLi |> Seq.toList)
    ]

(*
          li 
            []
            [ 
              div [ ClassName "collapsible-header"] 
                (mkTtile title1 "stars")
              div [ ClassName "collapsible-body"] 
                [ 
                  span [] [(getContents(c1))]
                ]
            ]
          li 
            []
            [
              div [ ClassName "collapsible-header"] 
                (mkTtile title2 "supervisor_account")
              div [ ClassName "collapsible-body"] 
                [ 
                  span [] [(getContents(c2))] 
                  img 
                    [ Src "https://www.ecodechets47.com/og2.png"; 
                      Alt "Organigramme"; 
                      ClassName "responsive-img z-depth-2 center s4"
                      unbox( "data-caption","organigramme") ]
                    [ ]
                ] 
            ]
          li 
            []
            [ 
              div [ ClassName "collapsible-header"] 
                (mkTtile title3 "assignment")
              div [ ClassName "collapsible-body"] 
                [ 
                  span [] [(getContents(c3))]
                ]
            ]            
          li 
            []
            [ 
              div [ ClassName "collapsible-header"] 
                (mkTtile title4 "web")
              div [ ClassName "collapsible-body"] 
                [ 
                  span [] [(getContents(c1))]
                ]
            ]            
  
*)