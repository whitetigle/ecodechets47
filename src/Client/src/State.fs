module App.State

open Elmish
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser
open Fable.Import.Browser
open Global
open Types

let initModel map = { 
  currentPage = Home; 
  initDone = false;
  map = Map.Types.initModel;
  collect = 0
}

let urlUpdate (result: Option<Page>) model =
  
  let model =
    match model.map.map with 
      | Some map -> 
        map
          .off()
          .remove() 
          |> ignore

        { model with map = { model.map with map = None } }
      | None -> model
    
  match result with
  | None ->
    console.error("Error parsing url")
    model,Navigation.modifyUrl (toHash model.currentPage)
  | Some page ->  
    { model with currentPage = page; collect = 0 }, []

let init result = 
  printfn "App init"   
  let (step, collectCmd) = Collect.State.init()
  let (map, mapCmd) = Map.State.init()
  let (model, cmd) = 
    urlUpdate result (initModel map)
  model, Cmd.batch [ cmd ]

let update msg model =
  printfn "App.update"
  match msg with
  | MapMsg msg ->
    let (mymap, cmd) = Map.State.update msg model.map 
    {model with map = mymap}, Cmd.map MapMsg cmd
  
  | NavMsg msg ->
    let (initDone, cmd) = Navbar.State.update msg model
    { model with initDone=true}, Cmd.map NavMsg cmd
  
  | CollectMsg msg ->
    let (step, cmd) = Collect.State.update msg model
    printfn "step=%i"step
    { model with collect=step}, Cmd.map CollectMsg cmd
