module Map.State

open Elmish
open Types
open Global

let init () : Model * Cmd<Msg> =
  printfn "Map.init"
  {map= None; markers=[]; topic=Glass}, []

let update msg model =
  printfn "Map.update"
  
  let currentMapState() = 
    match msg with 
    | MountMap map -> Some (map)
    | UnmountMap ->   None

  let model =  { model with map = currentMapState() }      
  model, []
