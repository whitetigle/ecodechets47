module Map.View

open Global

open Fable.Core
open Fable.Import.Browser
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Types
open Leaflet

let prepareMap model = 
  printfn "mount map"
  L.Mapbox.AccessToken <- "pk.eyJ1Ijoid2hpdGV0aWdsZSIsImEiOiJhZmMwZTcwMGEzYmQ2NDhlYTJiNmY3NTJkMWY0NjQ1ZiJ9.mftru4khn6x5BHeumyk7ug"
  
  let map = 
    L.Mapbox.Map("mapid","mapbox.streets")
      .SetView((44.504935,0.157350), 12)

  model.markers
    |> Seq.iter( fun (lat,long) -> 
      (L.Marker lat long).AddTo(map) |> ignore
    )            

  let marker = 
    (L.Marker 44.504995 0.157896)
      .AddTo(map)
      .BindPopup("Ici c'est le siège de la Micro Entreprise EcoDechets47 !")

  map 

let root model dispatch =
  let topicToString t = 
    match t with 
    | Glass -> "mon verre"
    | Clothes ->"mes vêtements"
    | _ -> ""

  div [ ClassName "center"]
    [
      h4 
        [ClassName "red-text text-lighten-2"]
        [str (sprintf "Où recycler %s?" (topicToString model.topic))]
      p [ ClassName "kanit"]
        [ str "Découvrez les emplacements des conteneurs de recyclage dans votre localité"]
      div
        [  
          Id "mapid" 
          Ref( 
            fun x ->
              if x <> null && model.map.IsNone then 
                MountMap (prepareMap model) |> dispatch            
          )
        ] []
    ]
