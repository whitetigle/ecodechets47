module Map.Types
 
open Leaflet

type Topic = Glass | Clothes

type Model = {
  map: L.IMap option
  markers : (float * float) list
  topic: Topic
}

type Msg =
  | MountMap of L.IMap
  | UnmountMap

let initModel = { map = None; markers = Global.glassGPS; topic = Glass } 
