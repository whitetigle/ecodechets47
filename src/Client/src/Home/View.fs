module Home.View

open Fable.Core
open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props

type Card = {
  title : string
  description : string
  link : string
  linkDescription : string
}

let cards = 
  [
    { title = "C'est quoi";
      description = "C'est un service de géolocalisation des containeurs de recyclage dans votre commune";
      link = "#about";
      linkDescription = "Plus d'informations"
    }
    { title = "Recyclage";
      description = "Trouvez le containeurs le plus proche de chez vous sur notre carte.";
      link = "#map";
      linkDescription = "Plus d'informations"
    }
    { title = "Encombrants";
      description = "Besoin de vous défaire d'un encombrant ? Indiquez à nos partenaires où il se trouve !";
      link = "#collect";
      linkDescription = "Plus d'informations"
    }
  ]

let makeCard card= 
  div
    [ ClassName "col s12 m12" ]
    [ div
        [ ClassName "card blue-grey darken-1" ]        
        [
          div
            [ ClassName "card-content white-text"]
            [
              span
                [ClassName "card-title"]
                [ str card.title ]
              p
                []
                [ str card.description ]
            ]
          div 
            [ClassName "card-action"]
            [
              a 
                [Href card.link ]
                [ str card.linkDescription ]
            ]
        ] ]
  

let root =
  div
    [
      ClassName "logo";
    ]            
    [
      div 
        [ClassName "col s12 center"] 
        [
        img 
          [ Src "https://www.ecodechets47.com/logo.png" 
            Alt "Logo"
            ClassName "circle responsive-img z-depth-2"
            Width (U2.Case1 500.0)
            unbox( "data-caption","Notre super logo !") ]
          [ ]
        ]
      div 
        [ClassName "col s12 center"] 
        [
          h4 [][str "Pour une planète sauvée !"]
        ]
    ]
    //(cards |> Seq.map makeCard |> Seq.toList)            
