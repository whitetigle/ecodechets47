module Navbar.State

open Elmish
open Types
open Global
open Fable.Core
open Fable.Core.JsInterop
open Fable.Import.Browser

[<Pojo>]
type NavOptions = {
  closeOnClick : bool
}

let init () : Model * Cmd<Msg> =
  printfn "Navbar.init"
  false, []

let update msg model =
  printfn "NavBar.update"
  
  let nav = Global.jQuery ".button-collapse"
  let options = { closeOnClick = true }
  nav ? sideNav ( options ) |> ignore  
      
  true, []
