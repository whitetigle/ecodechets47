module Navbar.View

open Global

open Fable.Core
open Fable.Import.Browser
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Types

let menuItem label page css icon (currentPage:Page) =
    let isActive = if page = currentPage then "active" else ""

    li
      [ ClassName (sprintf "%s" isActive) ]
      [ 
        a
          [ ClassName css; Href (toHash page) ]
          [ 
            i 
              [ ClassName (sprintf "fa fa-%s" icon ); unbox( "aria-hidden", "true") ]
              []
            str (sprintf " %s" label)
          ] 
      ]

let root initDone currentPage dispatch = 

  let initBar initDone element = 
    if element <> null && (not initDone) then 
      InitBar |> dispatch

  let menuItems cp = 
    [ 
      menuItem "Le projet" Info "blue-grey-text text-darken-4" "heart-o" cp
      menuItem "Mon verre" Map "blue-grey-text text-darken-4" "glass" cp
      menuItem "Mes Vêtements" Clothes "blue-grey-text text-darken-4" "user-o" cp
      menuItem "Mes encombrants" Collect "blue-grey-text text-darken-4" "truck" cp
      menuItem "Cycle des déchêts" SecondLife "blue-grey-text text-darken-4" "info-circle" cp
      menuItem "Espace Pro" Pro "blue-grey-text text-darken-4" "group" cp  
    ]

  div 
    [ Ref (initBar initDone) ]
    [ 
    nav 
      [ ClassName "light-green"  ]
      [ 
        div 
          [ ClassName "nav-wrapper" ]
          [ 
          a 
            [  Href "#home"; ClassName "kanit brand-logo"; ] 
            [ 
              i 
                [ ClassName "fa fa-recycle white-text"; unbox( "aria-hidden", "true") ]
                []
              str "EcoDecheTs47" 
            ] 
          a 
            [ Href "#"; ClassName "button-collapse"; 
              unbox ("data-activates", "mobile-demo") 
            ]
            [ 
              i [ ClassName "material-icons" ]
                [ str "menu" ]
            ] 
          ul (* LARGE MENU *)
            [ ClassName "right hide-on-med-and-down" ]
            (menuItems currentPage)
          ul (* MOBILE MENU *)
            [ ClassName "side-nav"; Id "mobile-demo" ]
            (menuItems currentPage)
      ] ] ]
