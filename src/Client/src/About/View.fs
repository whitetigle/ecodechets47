module About.View

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Browser

type Information = {
  Title : string
  Texts: string list
  Icon:string
}

let infoList = [
  { 
    Title = "Collégiens Entrepreneurs à 14 ans !"
    Icon = "stars"
    Texts = 
    [
      "Nous sommes seize mini-entrepreneurs de troisième au collège de la cité scolaire de Marmande. Nous sommes supervisés par l’association EPA (Entreprendre Pour Apprendre) et sommes accompagnés de deux professeurs encadrants ; Mme Fort et Mme Chaumel."
      "Nous avons également un parrain, M William Fort, qui est chef d’entreprise."
      "Une fois par semaine, le jeudi de 15h à 17h, nous nous réunissons pour travailler pleinement sur notre projet, en tant que mini entrepreneur dans l’entreprise « EcoDecheTs47 »"  
      "Notre entreprise est composée de six services : la direction générale, le service financier, le service administratif/relation humaine, le service de communication/marketing, le service technique et le service commercial/relation client."   
    ]
  }
  { 
    Title = "Les employés de la mini-entreprise !"
    Icon = "supervisor_account"
    Texts = 
    [
      "Travailler différemment et tous ensemble, c’est ce qui fait la force de notre projet." 
      "Notre mini-entreprise crée un cadre de travail très original.  Les élèves et les enseignants fabriquent pas à pas leur projet avec l’aide du parrain d’EcoDecheTs47 et d’un permanent de l’association EPA :" 
      "Les mini-entrepreneurs composés de 16 élèves de la classe des 3°2 créent une entreprise, ce qui est une aventure très enrichissante et tout aussi passionnante. Une expérience unique qu’ils partageront." 
      "L’équipe pédagogique composée de Mme Fort et Mme Chaumel, respectivement professeur de français et d’espagnol, elles s’impliquent et nous accompagnent dans notre projet qui est résolument pluridisciplinaire."  
      "M François Nicaise, consultant en Game Design et Développement Gamification, nous apporte son aide et ses conseils chaque semaine pour notre projet, bénévolement."  
      "Les entretiens d’embauches ont été effectués par M Fort, qui a été également conseillé par les enseignants encadrants."
      "M Fort, qui est chef d’entreprise, nous accompagne et partage son expérience et la passion qu’il a pour son métier, il s’associe à l’équipe pédagogique, et il intervient en complément des permanents de l’association EPA en tant que parrain."  
    ]
  }
  { 
    Title = "Notre Cadre Légal"
    Icon = "assignment"
    Texts = 
    [
      "La mini-entreprise n’existe pas juridiquement, elle est portée par l’association Entreprendre Pour Apprendre (EPA), qui supporte le risque financier lié au projet."
      "La comptabilité de l’entreprise est gérée via un compte bancaire mis à disposition par l’association EPA. En aucun cas, les flux financiers transitent par l’établissement."
    ]
  }
  { 
    Title = "Entreprendre pour Apprendre"
    Icon = "web"
    Texts = 
    [
      "Entreprendre pour apprendre, dit EPA, est une association créée en 1901, est indépendante, apolitique et aconfessionnelle depuis 2005.  EPA rassemble des acteurs du monde éducatif, de l’environnement économique et social, dans le domaine public et privé autour d’un projet commun." 
      "Elle nous fait découvrir le quotidien d’une vie en entreprise."
      "Elle nous permet de mieux percevoir les notions en gestion, en communication et marketing, en commercialisation et relation client, la fabrication et la production d’un service ou produit, dans le domaine de l’administratif et relation humaine ou encore tout simplement dans la gérance d’une équipe."
      "Elle nous développe des qualités telles que l’autonomie, la responsabilité, la confiance et la cohésion d’équipe."
      "C’est pour nous un nouveau dispositif pédagogique très différent des cours classiques, ce qui nous plaît tout particulièrement."
      "Elle nous sensibilise et nous donne la confiance nécessaire pour créer notre propre entreprise plus tard."
    ]
  }
]


let getContents clist = 
  div 
    []
    (clist 
      |> Seq.map( fun co -> 
          p
            [ ]
            [ str co ]  
        )
      |> Seq.toList)

let mkTtile t icon= 
  [
    i
      [ ClassName "material-icons" ]      
      [ str icon ]
    h4 
      []
      [str t]
  ]

let root =
  let initAccordion element = 
    let nav = Global.jQuery ".collapsible"
    nav ? collapsible () |> ignore  

  let mkLi inf = 
    li 
      []
      [ 
        div [ ClassName "collapsible-header kanit"] 
          (mkTtile inf.Title inf.Icon)
        div [ ClassName "collapsible-body"] 
          [ 
            span [] [(getContents(inf.Texts))]
          ]
      ]


  div
    [ ClassName "row center"]
    [
      h4 
        [ ClassName "red-text text-lighten-2"]
        [ str "EcoDecheTs47: notre Mini-Entreprise !"]
      p 
        [ ClassName "kanit"]
        [str "Découvrez notre projet au travers des rubriques suivantes."]
      ul
        [ ClassName "collapsible left-align"; 
          unbox("data-collapsible","accordion");
          Ref initAccordion 
        ]
        (infoList |> Seq.map mkLi |> Seq.toList)
    ]
