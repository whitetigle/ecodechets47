module Collect.View

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop
open Fable.Import.Browser
open Fable.Core
open Fable.Import.Browser

open Types

[<Pojo>]
type DateOptions = {
  selectMonths : bool
  selectYears : int
}


module Helper = 
  let mkCard icon title text button = 
    div [ ClassName "col s12 m4"]
        [
          div 
            [ClassName "card light-green lighten-4 small icon-block"]
            [
              h2  
                [ClassName "center brown-text" ]
                [ 
                  i 
                    [ ClassName (sprintf "fa fa-%s brown-text" icon); unbox( "aria-hidden", "true") ]
                    []
                ]    
              h5 [ ClassName "center" ] [ str title ]              
              p [ ClassName "light"] [ str text]
              div 
                [ClassName "card-action"]
                [ button ]
            ]
        ]

let recycleLocations model dispatch = 
  let mkButton url = 
    a 
      [ ClassName "waves-effect waves-light btn-large light-green"
        Href url
        Target "_blank"
      ]
      [ 
        i [ ClassName "fa fa-plug black-text left" ] []
        str "Voir la carte"
      ]
    
  [
    div
      [ ClassName "row" ]
      [ 
        h4 
          [ClassName "red-text text-lighten-2"]
          [str "Recycler ses déchêts électriques et électroniques"]
        p [ ClassName "kanit"]
          [ str "Recyclez vos Déchets électorniques facilement. Consultez les cartes ci-dessous pour trouver les lieux de recyclage les plus proches de vous !"]
        (Helper.mkCard "recycle" "Val de Garonne" "Découvrez les points de recyclage pour les déchets électriques et électroniques pour cette zone géographique" (mkButton "http://www.vg-agglo.com/-Decheteries-.html") )
        (Helper.mkCard "recycle" "Dordogne" "Découvrez les points de recyclage pour les déchets électriques et électroniques pour cette zone géographique" (mkButton "http://www.dechets-aquitaine.fr/solutions/?dpt=24&dcht=79&orgtype=") )
        (Helper.mkCard "recycle" "Lot & Garonne" "Découvrez les points de recyclage pour les déchets électriques et électroniques pour cette zone géographique" (mkButton "http://www.dechets-aquitaine.fr/solutions/?dpt=47&dcht=79&orgtype=") )
        (Helper.mkCard "recycle" "Gironde" "Découvrez les points de recyclage pour les déchets électriques et électroniques pour cette zone géographique" (mkButton "http://www.dechets-aquitaine.fr/solutions/?dpt=33&dcht=79&orgtype=") )
        (Helper.mkCard "recycle" "Landes" "Découvrez les points de recyclage pour les déchets électriques et électroniques pour cette zone géographique" (mkButton "http://www.dechets-aquitaine.fr/solutions/?dpt=40&dcht=79&orgtype=") )
        (Helper.mkCard "recycle" "Pyrénées Atlantiques" "Découvrez les points de recyclage pour les déchets électriques et électroniques pour cette zone géographique" (mkButton "http://www.dechets-aquitaine.fr/solutions/?dpt=64&dcht=79&orgtype=") )
      ]
  ]

let introduction model dispatch= 
  [
    h4 
      [ClassName "red-text text-lighten-2"]
      [str "Que faire de mes encombrants ?"]
    p [ ClassName "kanit"]
      [ str "Que vous souhaitiez les porter vous même dans une déchetterie ou les faire enlver par un professionnel, suivez notre assistant intéractif..."]
    a 
      [ ClassName "waves-effect waves-light btn-large light-green"
        Href "#collect"
        OnClick (fun _ -> Next(model) |> dispatch) 
      ]
      [ 
        i [ ClassName "material-icons left" ] [str "flash_on"]
        str "Démarrer l'assistant !"
      ]
  ]

let chooseKind model dispatch = 

  let buttonDEE = 
    a 
      [ ClassName "waves-effect waves-light btn-large light-green"
        OnClick (fun _ -> Next(10) |> dispatch) 
      ]
      [ 
        i [ ClassName "fa fa-plug black-text left" ] []
        str "Recycler !"
      ]

  let buttonMeubles = 
    a 
      [ ClassName "waves-effect waves-light btn-large light-green"
        OnClick (fun _ -> Next(model) |> dispatch) 
      ]
      [ 
        i [ ClassName "fa fa-truck black-text left" ] []
        str "Allons-y !"
      ]

  let buttonOther = 
    a 
      [ ClassName "waves-effect waves-light btn-large light-green"
        Href "#secondlife"
      ]
      [ 
        i [ ClassName "fa fa-recycle black-text left" ] []
        str " Guide des déchêts"
      ]

  [
    div
      [ ClassName "row" ]
      [ 
        h5 [ ClassName "kanit"]
          [ str "Choisissez le type d'encombrant dont vous souhaitez vous défaire."]

        (Helper.mkCard "truck" "Meubles" "Je souhaite faire enlever un meuble (canapé, armoire, etc...)" buttonMeubles)
        (Helper.mkCard "plug" "Déchêts Electriques (DEEE)" "Je souhaite me défaire d'un encombrant électrique ou électronique (refrigérateur, congélateur, etc...)" buttonDEE)
        (Helper.mkCard "recycle" "Autre" "Je ne sais pas exactement si c'est un encombrant (en tout cas c'est un déchêt)" buttonOther) 
      ]
  ]


[<Emit("Materialize.toast($0,$1)")>]
let makeToast text ms = jsNative

let myForm model dispatch =
  let mkInput id lab =
    div [ ClassName "input-field"]
      [
        input [ Placeholder "??"; Id id;Type "text"; ClassName "validate" ] []
        label [ unbox("for",id) ] [ str lab ]
      ]
    
  let initPicker element = 
    let picker = Global.jQuery ".datepicker"
    let options = { selectMonths = true; selectYears = 2 }
    picker ? pickadate (options) |> ignore  

  [
    h4 
      [ClassName "red-text text-lighten-2"]
      [str "Prendre Rendez-vous !"]
    p [ ClassName "kanit"]
      [ str "Entrez vos coordonnées ainsi qu'une date de collecte souhaitée. Nous nous chargerons de prévenir nos partenaires. Ils vous recontacteront dès que possible !"]

    div [ ClassName "center-align"]
      [
      div [ ClassName "col s12 m6"]
        [
          form [ ClassName ""]
            [
              div [ ClassName "row"]
                [
                  mkInput "what" "Quel encombrant souhaitez-vous faire enlever ?"
                ]
              div [ ClassName "row"]
                [
                  mkInput "first_name" "Prénom"
                ]
              div [ ClassName "row"]
                [
                  mkInput "last_name" "Nom"
                ]
              div [ ClassName "row"] 
                [ 
                  mkInput "adress1" "Adresse 1" 
                ]
              div [ ClassName "row"] 
                [ 
                  mkInput "adress2" "Adresse 2" 
                ]
              div [ ClassName "row"]
                [
                  mkInput "cp" "Code Postal"
                ]
              div [ ClassName "row"]
                [
                  mkInput "city" "Ville"
                ]
              div 
                [ 
                  ClassName "row"
                  Ref initPicker
                ]
                [ 
                  input [ Placeholder "Appuyez ici pour choisir une date";Type "date"; Id "date"; ClassName "datepicker" ] [] 
                  label [ unbox("for","date") ] [ str "Date de collecte souhaitée ?" ]
                ]
              div 
                [ 
                  ClassName "row"
                ]
                [ 
                  a 
                    [ ClassName "waves-effect waves-light btn-large light-green"
                      OnClick (fun _ -> 
                        makeToast "Demande enregistrée ! Merci et à bientôt !" 3000
                        Next(-1) |> dispatch
                      ) 
                    ]
                    [ 
                      i [ ClassName "fa fa-thumbs-up black-text left" ] []
                      str "Envoyez votre demande !"
                    ]
                ]
            ]
        ]
      ]
  ]
let getView model dispatch = 
  match model with 
  | 0 -> introduction model dispatch
  | 1 -> chooseKind model dispatch
  | 2 -> myForm model dispatch
  | 11 -> recycleLocations model dispatch
  | _ ->
    [ 
      p [][str "choix inconnu ??!!"]   
    ]

let root model dispatch =
  div
    [ ClassName "center" ]
    (getView model dispatch)
