module Collect.State

open Elmish
open Types
open Global

let init () : Model * Cmd<Msg> =
  printfn "Collect.init"
  0, []

let update msg model =
  printfn "Collect.update"
  
  let getNextStep() = 
    match msg with 
    | Next index -> index + 1

  let model = getNextStep()     
  model, []
