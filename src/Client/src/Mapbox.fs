namespace Leaflet

module L =

  open Fable.Core.JsInterop
  open Fable.Core

  type IMyOptions =
      interface end

  type MapOptions = 
    | ZoomControl of bool 
    | ScrollWheelZoom of bool
    interface IMyOptions

  type IMap = 
    abstract off : unit -> IMap
    abstract remove : unit -> IMap
    [<Emit("$0.setView($1,$2)")>]
    abstract SetView : (float * float) * int -> IMap

  type Mapbox = 
    [<Emit("L.mapbox.accessToken=$0")>]
    static member AccessToken with set(token: string) : unit  = jsNative
    [<Emit("L.mapbox.map($0,$1,$2)")>]
    static member Map(element: string, id: string, ?options: MapOptions): IMap = jsNative

  type IMarker = 
    [<Emit("$0.addTo($1)")>]
    abstract AddTo : IMap -> IMarker
    [<Emit("$0.bindPopup($1)")>]
    abstract BindPopup : string -> IMarker

  [<Emit("L.marker([$0,$1])")>]
  let Marker (lat:float) (long:float) : IMarker = jsNative
