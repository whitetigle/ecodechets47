module Pro.View

open Fable.Helpers.React
open Fable.Helpers.React.Props

let root =
  let mkCard icon title text = 
    div [ ClassName "col s12 m4"]
        [
          div 
            [ClassName "icon-block"]
            [
              h2  
                [ClassName "center brown-text" ]
                [ i 
                    [ ClassName "material-icons" ]
                    [ str icon]
                ]    
              h5 [ ClassName "center" ] [ str title ]              
              p [ ClassName "light"] [ str text]
            ]
        ]

  div
    [ ClassName "center" ]
    [
      div
        [ ClassName "row" ]
        [ 
          h4 
            [ClassName "red-text text-lighten-2"]
            [ str "Service Pro"]
          p [ ClassName "kanit"]
            [ str "Professionnels, retrouvez dans votre espace pro, les demandes de collecte d'encombrants dans votre région."]
          mkCard "location_on" "Votre carte interactive" "Abonnez-vous aux départements de votre choix et retrouvez les demandes de collecte géolocalisées."
          mkCard "schedule" "Vos demandes" "Prenez contact avec les particuliers pour la collecte directement depuis notre interface."
          mkCard "thumb_up" "Essai gratuit !" "Testez notre système gratuitement pendant trente jour puis, si vous êtes satisfait-e, abonnez-vous tout simplement continuer à utiliser l'intégralité de nos services."
        ]
      a 
        [ ClassName "waves-effect waves-light btn-large light-green"
          Href "mailto:ecodechets47@gmail.com?subject=Demande d'informations pour l'espace PRO" ]
        [ 
          i [ ClassName "material-icons left" ] [str "flash_on"]
          str "Je veux plus d'informations !"
        ]
    ]
