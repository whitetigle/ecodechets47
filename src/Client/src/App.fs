module App.View

open Elmish
open Elmish.Browser.Navigation
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Elmish.React
open App.Types
open App.State


(* our CSS
*)
open Fable.Core.JsInterop
importAll "../sass/main.sass"

(* Starting view
*)
open Global

let root model dispatch =

  let pageHtml =
    function
    | SecondLife ->  SecondLife.View.root
    | Info ->  About.View.root
    | Home -> Home.View.root //model.home (HomeMsg >> dispatch)
    | Collect -> Collect.View.root model.collect (CollectMsg >> dispatch)
    | Help -> Help.View.root //model.home (HomeMsg >> dispatch)
    | Map -> Map.View.root  { model.map with markers=glassGPS; topic=Map.Types.Topic.Glass} (MapMsg >> dispatch)
    | Clothes -> Map.View.root  { model.map with markers=clothesGPS; topic=Map.Types.Topic.Clothes} (MapMsg >> dispatch)
    | Pro -> Pro.View.root //model.home (HomeMsg >> dispatch)

  div 
    []
    [ 
      main 
        [ClassName "white"]
        [
          (* Top menu *)
          Navbar.View.root model.initDone model.currentPage (NavMsg >> dispatch)
          (* Contents *)
          div
//            [ ClassName "container" ]
            [ ClassName "" ]
            [ pageHtml model.currentPage ]
        ]
    ]


(* Parse our links into browsable pages  
  Tranform our #tags into relevant model
*)
open Elmish.Browser.UrlParser
let pageParser: Parser<Page->Page,Page> =
  oneOf [
    map SecondLife (s "secondlife")
    map Info (s "about")
    map Home (s "home")
    map Collect (s "collect")
    map Map (s "map")
    map Help (s "help")
    map Pro (s "pro")
    map Clothes (s "clothes")
  ]

(* Our App
*)
Program.mkProgram init update root
|> Program.toNavigable (parseHash pageParser) urlUpdate
|> Program.withReact "elmish-app"
|> Program.run
