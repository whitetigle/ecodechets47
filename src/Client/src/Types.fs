module App.Types

open Global

type Msg =
  | MapMsg of Map.Types.Msg
  | NavMsg of Navbar.Types.Msg
  | CollectMsg of Collect.Types.Msg

type Model = {
  currentPage: Page
  initDone : bool
  map: Map.Types.Model
  collect: Collect.Types.Model
}
