module Global

open System
open Fable.Core.JsInterop
open Fable.Core
open Fable.Import.Browser
open Fable.Import.JS


[<Emit("jQuery($0)")>]
let jQuery(x: obj): obj = jsNative

let clothesGPS = [
  44.489240, 0.181050
  44.491810, 0.176672
  44.503827, 0.183906
  44.498123, 0.168728
  44.501452, 0.168994
  44.503931, 0.163249
  44.508581, 0.160429
  44.503467, 0.154101
  44.507820, 0.152905
  44.507940, 0.147831
  44.510845, 0.137061
  44.512934, 0.132630
  44.515667, 0.124842
]

let glassGPS = [
  44.503910, 0.184004
  44.500904, 0.177070
  44.502730, 0.174981
  44.509593, 0.175474
  4.501623, 0.168893
  44.498866, 0.169002
  44.496626, 0.167473
  44.498400, 0.161044
  44.500080, 0.159088
  44.502335, 0.163723
  44.503669, 0.162970
  44.505732, 0.161216
  44.506561, 0.158106
  44.509646, 0.175446
  44.509739, 0.165960
  44.511340, 0.159947
  44.508854, 0.160208
  44.507925, 0.152341
  44.504573, 0.148907
  44.507915, 0.146558
]

type Page =
  | Home
  | Info
  | Map
  | Collect
  | Help
  | Pro
  | Clothes
  | SecondLife

let toHash =
  function
  | Info -> "#about"
  | Home -> "#home"
  | Map -> "#map"
  | Collect -> "#collect" 
  | Help -> "#help"
  | Pro -> "#pro"
  | Clothes -> "#clothes"
  | SecondLife -> "#secondlife"

