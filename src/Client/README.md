## Build and running the app

1. Install npm dependencies: `yarn install` or `npm install`
2. Install dotnet dependencies: `dotnet restore`
3. Start Fable server and Webpack dev server: `dotnet fable npm-run start`
4. In your browser, open: http://localhost:8082/

Any modification you do to the F# code will be reflected in the web page after saving.

# Install
dotnet restore
npm install

## Install from scratch (only needed to start again from scratch)
dotnet new -i Fable.Template.Elmish.React::*
dotnet new fable-elmish-react
dotnet fable add fable-powerpack@next fable-react@next fable-elmish@next fable-elmish-browser@next fable-elmish-debugger@next fable-elmish-react@next
npm install

# build
dotnet fable npm-run build