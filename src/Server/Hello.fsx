#I "packages/Suave/lib/net40"
#r "Suave.dll"

open Suave // always open suave
open Suave.Web // for config
open System.Net
open System.IO
open Suave.Filters
open Suave.Operators

let app : WebPart =
  GET >=> choose [
    path "/" >=> Files.browseFileHome "index.html"
    pathRegex @"/(public|js|css|img)/(.*)\.(css|png|gif|jpg|js|map)" >=> Files.browseHome
    path "/prout" >=> Successful.OK "prout"
    Files.browseHome
    RequestErrors.NOT_FOUND "Page not found. Dumb" 
  ]
  
let config = 
    { defaultConfig with
       bindings = [ HttpBinding.createSimple HTTP "127.0.0.1" 8085 ]
       homeFolder = Some (Path.GetFullPath "./public")
    }
  
printfn "path %s" (Path.GetFullPath "./public")
startWebServer config app
