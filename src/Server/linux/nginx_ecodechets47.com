# Default server configuration
#
server {
        listen 80;
        listen [::]:80;

        server_name www.echodechets47.com;
        return       301 https://www.ecodechets47.com$request_uri;
}

server {
        # SSL side
        listen 443;
#       listen [::]:443;

        ssl on;
        ssl_certificate  /home/ubuntu/ecodechets47/src/Server/linux/server.crt;
        ssl_certificate_key  /home/ubuntu/ecodechets47/src/Server/linux/myserver.key;

        server_name www.echodechets47.com;

        location / {
          proxy_pass http://127.0.0.1:8085;
        }
}
